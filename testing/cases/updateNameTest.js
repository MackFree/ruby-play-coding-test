setup = require('./aux/setup');
makeid = require('./aux/makeid');
require('chromedriver');
const assert = require('assert');
var webdriver = require('selenium-webdriver');

module.exports = function updateNameTest(username, password, flowBeingTested){
  let driver;

  before(async function() {
    driver = setup();
  });
  // Next, we will write steps for our test.
  // For the element ID, you can find it by open the browser inspect feature.
  it(flowBeingTested, async function() {
      // Load the page
      await driver.navigate().to('https://play.test.wildrubycasino.com/');
      //navigate to the login page
      await driver.findElement(webdriver.By.id('login-button')).click();
      //now enter the inforamtion (user name)
      await driver.findElement(webdriver.By.id('username')).sendKeys(username);
      //now for password
      await driver.findElement(webdriver.By.id('password')).sendKeys(password, webdriver.Key.RETURN);

      //success case and go on to change the display name
      await driver.findElement(webdriver.By.id('profile-button')).click();
      //now we change the name to a random string and confirm
      let randomName = makeid(10);
      await driver.findElement(webdriver.By.className('q-field row no-wrap items-start q-input q-field--outlined q-field--labeled q-field--dark')).click();
      //needs to be selected before it can be found
      await driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div[2]/main/div/div/p[3]/label/div/div/div/input')).sendKeys(randomName);

      await driver.findElement(webdriver.By.id('display-name-button')).click();
      //now we check name has been changed
      //hacky i know
      await driver.sleep(1000);
      let displayNameNew = await driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/div[2]/main/div/div/div/div[2]/p[2]')).getText();
      assert.equal(displayNameNew, 'Display Name: ' + randomName);
  });
}
