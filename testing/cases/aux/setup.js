require('chromedriver');
var webdriver = require('selenium-webdriver');

module.exports = function setup(){
  var chromeCapabilities = webdriver.Capabilities.chrome();
  var chromeOptions = {'args': ['--test-type', '--incognito']};
  chromeCapabilities.set('chromeOptions', chromeOptions);
  driver = new webdriver.Builder().withCapabilities(chromeCapabilities).build();
  return driver;
}
