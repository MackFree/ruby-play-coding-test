require('chromedriver');
const assert = require('assert');
var webdriver = require('selenium-webdriver');

module.exports = async function loginSuccessTest(driver){
  //checking if the data is correct
  let title = await driver.getTitle();
  let description = await driver.findElement(webdriver.By.xpath("//meta[@name='description']")).getAttribute('content');
  let gamesHeader = await driver.findElement(webdriver.By.xpath('/html/body/div[1]/div/header/div/div[4]')).getText();
  assert.equal(title, 'Wild Ruby Casino');
  assert.equal(description, 'Wild Ruby Casino Real Money Online Gaming');
  assert.equal(gamesHeader, 'GAMES');
}
