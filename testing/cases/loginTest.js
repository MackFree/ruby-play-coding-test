setup = require('./aux/setup');
loginSuccessTest = require('./loginSuccessTest')
require('chromedriver');
const assert = require('assert');
var webdriver = require('selenium-webdriver');

module.exports = function loginTest(username, password, successCondition, flowBeingTested) {
    //defining driver
    let driver;
    //setting up driver
    before(async function() {
      driver = setup();
    });

    it(flowBeingTested, async function() {
        // Load the page
        await driver.navigate().to('https://play.test.wildrubycasino.com/');
        //navigate to the login page
        await driver.findElement(webdriver.By.id('login-button')).click();
        //now enter the inforamtion (user name)
        await driver.findElement(webdriver.By.id('username')).sendKeys(username);
        //now for password
        await driver.findElement(webdriver.By.id('password')).sendKeys(password, webdriver.Key.RETURN);
        //now check the next page for element with the right id
        switch (successCondition) {
          case 0:
            //case 0 is a sucessful login
            await loginSuccessTest(driver);
            break;
          case 1:
            //case 1 is an unsucessful login
            let errMessage = await driver.findElement(webdriver.By.className('error-message roboto-condensed')).getText();
            //also check fields have been cleared
            assert.equal(errMessage, 'Check yourself. The email and password combo doesn\'t match.');
            break;
        }
    });
    // close the browser after running tests
}
