require('chromedriver');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
loginTest = require('./cases/loginTest')
updateNameTest = require('./cases/updateNameTest');

async function runTests(){
  await loginTest('test@rubyplaynetwork.com', 'wrongpassword', 1, 'Right Email Wrong Password');
  await loginTest('test1@rubyplaynetwork.com', 'wrongpassword', 1, 'Wrong Email Wrong Password');
  await loginTest('test1@rubyplaynetwork.com', 'Ruby1234', 1, 'Wrong Email Right Password');
  await loginTest('test@rubyplaynetwork.com', 'Ruby1234', 0, 'Right Email Right Password, Check Success only');
  await updateNameTest('test@rubyplaynetwork.com', 'Ruby1234', 'Right Email, Right Password, Check change user');
}

runTests();
